package Client;


import Core.Movie;
import Core.ServiceDetails;
import Core.User;
import Server.MovieDAOInterface;
import Server.MySqlUserDAO;
import Server.MysqlMovieDAO;
import Server.UserDAOInterface;
import Exceptions.DAOException;
import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.sql.SQLException;
import java.util.*;
import java.util.List;


public class Client {

    private static boolean isUserLoggedIn = false;
    private static User userCurrentlyLoggedIn;

    private static Socket dataSocket;
    private static OutputStream out;
    private static PrintWriter output;
    private static InputStream in;
    private static Scanner input;
    private static Scanner keyboard;
    private static String message;
    private static String response;

    //enums



    public static void main(String[] args) {

        try {
            dataSocket = new Socket("127.0.0.1", ServiceDetails.SERVER_PORT);
            out = dataSocket.getOutputStream();
            output = new PrintWriter(new OutputStreamWriter(out));

            in = dataSocket.getInputStream();
            input = new Scanner(new InputStreamReader(in));

            keyboard = new Scanner(System.in);

            message = "";

            while (!message.equals(ServiceDetails.ENDSESSION)) {
                displayMenu();
                int choice = keyboard.nextInt();
                response = "";

                if (choice >= 0 && choice <= 2) {
                    //Choice 0 Ends Session
                    if (choice == 0) {
                        closeConnection();
                    }
                    //Choice 1 starts User Queries which
                    else if (choice == 1) {
                        UserQueries();
                    }

                    //Choice 2 starts Movie Queries
                    else if (choice == 2) {
                        MovieQueries();
                    }
                } else {
                    System.out.println("Invalid character. Please select a number from 0-2");
                }
                if (response.equals(ServiceDetails.UNRECOGNIZED)) {
                    System.out.println("---------------------------------------------------------");
                    System.out.println("|        Sorry unrecognized. Please try again.          |");
                    System.out.println("---------------------------------------------------------");
                }
            }
            System.out.println("------------------");
            System.out.println("|  socketClosed  |");
            System.out.println("------------------");
            dataSocket.close();
        } catch (Exception e) {
            System.out.println("Error code: " + e.getMessage());
            e.printStackTrace();
        }
    }


    //--------------------------------------------MAIN FINISHES HERE-------------------------------------------------------
    //--------------------------------------------MAIN FINISHES HERE-------------------------------------------------------
    //--------------------------------------------MAIN FINISHES HERE-------------------------------------------------------

    public static void displayMenu() {
        System.out.println("----------------------------------------------------------------------");
        System.out.println("--------    Welcome to our TCP Client-Server Application    ----------");
        System.out.println("----------      Please Select your desired option:      --------------");
        System.out.println("----------------------------------------------------------------------");
        System.out.println("-----------------------   0. Exit           --------------------------");
        System.out.println("-----------------------   1. User Queries   --------------------------");
        System.out.println("-----------------------   2. Movie Queries  --------------------------");
        System.out.println("----------------------------------------------------------------------");
    }

    //User Queries method is suppose to call all other methods which deal with the User
    public static void UserQueries() throws SQLException, IOException {
        int userQueriesKB;
        final int backtomenu = 0;
        System.out.println("-------You've selected User Queries.-------");
        System.out.println("Now choose what would you like to do: ");
        System.out.println("-------------------------------------------");
        System.out.println("To return to the main Menu press 0");
        if( isUserLoggedIn != false) {
            System.out.println("1. displayAllUsers:");
            System.out.println("2. testing recommendation:");
            System.out.println("3. Remove");
            System.out.println("4. Update");
        }
        System.out.println("5. Login");
        System.out.println("6. Register:");
        System.out.println("-------------------------------------------");
        System.out.println("is user currently logged in: " + isUserLoggedIn);

        Scanner kb = new Scanner(System.in);
        userQueriesKB = kb.nextInt();
        switch(userQueriesKB) {
            case (backtomenu):
                break;

            case(1):
                message = displayAllUsers();
                output.println(message);
                output.flush();
                while(input.hasNext()) {
                    response = input.next();
                }
                System.out.println(response);
                break;

            case(2):
                message = pickSomeGenresForRecommendations();
                output.println(message);
                output.flush();
                while(input.hasNextLine()) {
                    response = input.nextLine();
                }
                System.out.println(response);
                break;

            case(3):
                message = removeUser();
                output.println(message);
                output.flush();
                while(input.hasNextLine()) {
                    response = input.nextLine();
                }
                System.out.println(response);
                keyboard.close();
                kb.close();

                break;
            case(4):
                message = updateUser();
                output.print(message);
                output.flush();
                while(input.hasNextLine()) {
                    response = input.nextLine();
                }
                System.out.println(response);
                break;

            case(5):
                message = logIn();
                output.print(message);
                output.flush();
                while(input.hasNextLine()) {
                    response = input.nextLine();
                }
                System.out.println(response);
                break;

            case(6):
                message = addNewUser();
                output.println(message);
                output.flush();
                while(input.hasNextLine()) {
                    response = input.nextLine();
                }
                System.out.println(response);
                break;

            default:
                System.out.println("not a valid choice");
                break;
        }
    }

    //Movie Queries method is suppose to call all other methods which deal with the movies
    public static void MovieQueries() throws SQLException {

        int movieQueriesKB;
        final int backtomenu = 0;
        System.out.println("-------You've selected Movie Queries.-------");
        System.out.println("Now choose what would you like to do: ");
        System.out.println("-------------------------------------------");
        System.out.println("To return to the main Menu press 0");
        System.out.println("1. Search Movie");
        System.out.println("2. displayAllFilms");
        if (isUserLoggedIn != false) {
            System.out.println("3. recommendFilmByGenre");
            System.out.println("4. Add new Movie");
            System.out.println("5. Update Movie");
            System.out.println("6. Remove Movie");
        }
        System.out.println("-------------------------------------------");
        Scanner kb = new Scanner(System.in);
        movieQueriesKB = kb.nextInt();

        switch (movieQueriesKB) {

            case (backtomenu):
                displayMenu();
                break;

            case (1):
                System.out.println("please input a genre: ");
                String genre = input.nextLine();
                message = searchGenre(genre);
                output.println(message);
                output.flush();
                while(input.hasNextLine()) {
                    response = input.nextLine();
                }
                System.out.println(response);
                break;

            case (2):
                message = displayAllFilms();
                output.println(message);
                output.flush();
                while(input.hasNext()) {
                    response = input.next();
                }
                System.out.println(response);
                break;

            case(3):
                message = pickSomeGenresForRecommendations();
                output.println(message);
                output.flush();
                while(input.hasNextLine()) {
                    response = input.nextLine();
                }
                System.out.println(response);
                break;
            case(4):
                message = addNewMovie();
                output.println(message);
                output.flush();
                while(input.hasNextLine()) {
                    response = input.nextLine();
                }
                System.out.println(response);
                break;

            case(5):
                message = updateMovie();

                output.println(message);
                output.flush();
                while(input.hasNextLine()) {
                    response = input.nextLine();
                }
                System.out.println(response);
                break;

            case(6):
                removeMovie();
                break;

            default:
                break;
        }
    }

    //----------------------------------------USERMENUS------------------------------------------------------
    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------


    public static String addNewUser() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Username");
        String uname = kb.nextLine();
        System.out.println("password");
        String pword = kb.nextLine();
        System.out.println("first name");
        String fname = kb.nextLine();
        System.out.println("last name");
        String lname = kb.nextLine();


        StringBuffer message = new StringBuffer(ServiceDetails.REGISTERUSER + ServiceDetails.BREAKING_CHARACTERS);
        message.append(uname + ServiceDetails.BREAKING_CHARACTERS + pword +
                ServiceDetails.BREAKING_CHARACTERS + fname + ServiceDetails.BREAKING_CHARACTERS +
                lname + ServiceDetails.BREAKING_CHARACTERS);

        return message.toString();
    }

    public static String updateUser() {
        Scanner kb = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);
        StringBuffer message = new StringBuffer(ServiceDetails.UPDATEUSER + ServiceDetails.BREAKING_CHARACTERS);
        System.out.println("user name to update: ");
        String userToUpdate = sc.nextLine();
        message.append(userToUpdate + ServiceDetails.BREAKING_CHARACTERS);
        System.out.println("1. Update Username:");
        System.out.println("2. Update Password:");
        System.out.println("3. update firstname:");
        System.out.println("3. update secondname:");
        int choice = kb.nextInt();
        switch(choice){
            case(1):
                System.out.println("new username:");
                String newuser = sc.nextLine();
                message.append(newuser + ServiceDetails.BREAKING_CHARACTERS);
                int one = 1;
                message.append(one + ServiceDetails.BREAKING_CHARACTERS);
                break;
            case(2):
                System.out.println("new password:");
                String newpassword = sc.nextLine();
                message.append(newpassword + ServiceDetails.BREAKING_CHARACTERS);
                int two = 2;
                message.append(two + ServiceDetails.BREAKING_CHARACTERS);
                break;
            case(3):
                System.out.println("new firstname:");
                String newfname = sc.nextLine();
                message.append(newfname + ServiceDetails.BREAKING_CHARACTERS);
                int three = 3;
                message.append(three + ServiceDetails.BREAKING_CHARACTERS);
                break;
            case(4):
                System.out.println("new lastname:");
                String newlname = kb.nextLine();
                message.append(newlname + ServiceDetails.BREAKING_CHARACTERS);
                int four = 4;
                message.append(four + ServiceDetails.BREAKING_CHARACTERS);
                break;
        }
        return message.toString();
    }

    public static String displayAllUsers() {
        StringBuffer message = new StringBuffer(ServiceDetails.FINDALLUSERS + ServiceDetails.BREAKING_CHARACTERS);
        return message.toString();
    }

    public static String removeUser(){
        StringBuffer message = new StringBuffer(ServiceDetails.REMOVEUSER + ServiceDetails.BREAKING_CHARACTERS);

        Scanner kb = new Scanner(System.in);
        System.out.println("type the userID of the user you would like to remove: ");
        String userToDelete = kb.nextLine();
        System.out.println("type password for that userID");
        String passToDelete = kb.nextLine();

        kb.close();
        message.append(userToDelete + ServiceDetails.BREAKING_CHARACTERS + passToDelete + ServiceDetails.BREAKING_CHARACTERS);
        return message.toString();
    }

    public static String logIn() throws DAOException {
        //takes the user name and pword from the user.
        //creates a new user with these details then send to checkIfUserExists()
        //check if user exists queries and returns a boolean.
        //if returned true - set logged in status to be the user details
        //else return false - break;

        Scanner sc = new Scanner(System.in);
        System.out.println("Username: ");
        String uname = sc.nextLine();
        System.out.println("Password: ");
        String pword = sc.nextLine();
        StringBuffer message = new StringBuffer(ServiceDetails.LOGIN + ServiceDetails.BREAKING_CHARACTERS);
        message.append(uname + ServiceDetails.BREAKING_CHARACTERS + pword + ServiceDetails.BREAKING_CHARACTERS);
        User user = new User(uname, pword);


        String loggedIn = "";

        if (checkIfUserExists(uname,pword) == false) {
            message.append("FALSE");
            setLoggedInStatus(false);
            System.out.println(loggedIn = "not logged in, please try again");
            userCurrentlyLoggedIn = null;
        } else if (checkIfUserExists(uname, pword) == true) {
            message.append("TRUE");
            setLoggedInStatus(true);
            userCurrentlyLoggedIn = user;
            System.out.println(loggedIn = "1 logged in as:  " + uname);
        }
        else{
            System.out.println("could not read data: line 387");
        }
        return message.toString();
    }

    public static boolean checkIfUserExists(String uName, String pWord) throws DAOException {
        //send the user, uname and pword that have to be compared to the db, gets the response and if response returns true
        // then set logged in status to true;
        boolean doesUserExist = true;

        StringBuffer message = new StringBuffer(ServiceDetails.CHECKIFUSEREXISTS + ServiceDetails.BREAKING_CHARACTERS);
        message.append(uName + ServiceDetails.BREAKING_CHARACTERS + pWord + ServiceDetails.BREAKING_CHARACTERS);
        output.println(message);
        output.flush();

        response = input.next();
        if (response == String.valueOf(true)){
            System.out.println(response);
            doesUserExist = true;
        }
        if (response == String.valueOf(false)){
            System.out.println(response);
            doesUserExist = false;
        }
        else{
            System.out.println("failed");
        }
        return doesUserExist;
    }

    public static Boolean setLoggedInStatus(boolean logged) {
        isUserLoggedIn = logged;
        return isUserLoggedIn;
    }


    //----------------------------------------------FILM MENUS------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------


    public static String displayAllFilms() {
        StringBuffer message = new StringBuffer(ServiceDetails.FINDALLFILMS + ServiceDetails.BREAKING_CHARACTERS);
        return message.toString();
    }

    public static String searchGenre(String movieGenreInput) throws NullPointerException {
        StringBuffer message = new StringBuffer(ServiceDetails.SEARCHGENRE + ServiceDetails.BREAKING_CHARACTERS);
        message.append(movieGenreInput + ServiceDetails.BREAKING_CHARACTERS);
        return message.toString();
    }

    public static String addNewMovie() {

        Scanner sc = new Scanner(System.in);
        System.out.println("title: ");   String TITLE    = sc.nextLine();
        System.out.println("genre: ");   String GENRE    = sc.nextLine();
        System.out.println("director: ");String DIRECTOR = sc.nextLine();
        System.out.println("runtime: "); String RUNTIME  = sc.nextLine();
        System.out.println("plot: ");    String PLOT     = sc.nextLine();
        System.out.println("location: ");String LOCATION = sc.nextLine();
        System.out.println("rating: ");  String RATING   = sc.nextLine();
        System.out.println("format: ");  String FORMAT   = sc.nextLine();
        System.out.println("year: ");    String YEAR     = sc.nextLine();
        System.out.println("starring: ");String STARRING = sc.nextLine();
        System.out.println("copies: ");  int COPIES      = sc.nextInt();

        StringBuffer message = new StringBuffer(ServiceDetails.ADDMOVIE + ServiceDetails.BREAKING_CHARACTERS);
        message.append(TITLE + ServiceDetails.BREAKING_CHARACTERS +
                GENRE + ServiceDetails.BREAKING_CHARACTERS +
                DIRECTOR + ServiceDetails.BREAKING_CHARACTERS +
                RUNTIME + ServiceDetails.BREAKING_CHARACTERS +
                PLOT +ServiceDetails.BREAKING_CHARACTERS +
                LOCATION +ServiceDetails.BREAKING_CHARACTERS +
                RATING +ServiceDetails.BREAKING_CHARACTERS +
                FORMAT +ServiceDetails.BREAKING_CHARACTERS +
                YEAR +ServiceDetails.BREAKING_CHARACTERS +
                STARRING +ServiceDetails.BREAKING_CHARACTERS +
                COPIES +ServiceDetails.BREAKING_CHARACTERS);
        output.println(message);
        output.flush();

//        Movie movie = IMovieDAO.addMovie(TITLE, GENRE, DIRECTOR, RUNTIME, PLOT, LOCATION, RATING, FORMAT, YEAR, STARRING, COPIES);
//        System.out.println("movie successfully created!");

        return message.toString();
    }

    public static String removeMovie()  {
        StringBuffer message = new StringBuffer(ServiceDetails.REMOVEMOVIE + ServiceDetails.BREAKING_CHARACTERS);
        String movieToRemove;
        System.out.println("Title of film to update: ");
        movieToRemove = keyboard.nextLine();

        message.append(movieToRemove);
        output.println(message);
        output.flush();

        return message.toString();
    }

    public static String updateMovie() {
        StringBuffer message = new StringBuffer(ServiceDetails.UPDATEFILM + ServiceDetails.BREAKING_CHARACTERS);
        String movieToUpdate;
        System.out.println("Title of film to update: ");
        movieToUpdate = keyboard.nextLine();

        message.append(movieToUpdate + ServiceDetails.BREAKING_CHARACTERS);
        output.println(message);
        output.flush();

        return message.toString();
    }

    //creates a linked list to store 5 genres that the user would like us to
    //recommend to them, kind of a jumping off point and should be
    //prompted to the user when they first register.
    public static String pickSomeGenresForRecommendations() {
        StringBuffer message = new StringBuffer(ServiceDetails.RECOMMEND + ServiceDetails.BREAKING_CHARACTERS);

        System.out.println("select the genres you would like me to " +
                "recommend to you");

        List<String> genresChosen = new ArrayList<String>();

        int count;
        boolean running = true;
        List<String> genreList = new ArrayList<String>();
        System.out.println("1.  Comedy");
        genreList.add("comedy");
        System.out.println("2.  Action");
        genreList.add("action");
        System.out.println("3.  Drama");
        genreList.add("drama");
        System.out.println("4.  Thriller");
        genreList.add("thriller");
        System.out.println("5.  Horror");
        genreList.add("horror");
        System.out.println("6.  Sci-Fi");
        genreList.add("scifi");
        System.out.println("7.  Documentary");
        genreList.add("documentary");
        System.out.println("8.  Romance");
        genreList.add("romance");
        System.out.println("9.  Indie");
        genreList.add("indie");
        System.out.println("10. Super Hero");
        genreList.add("super");

        System.out.println("Press 0. to break");
        while (running != false) {

            int input = keyboard.nextInt();
            if (input <= 10) {

                if (input == 0) {
                    if (genresChosen.size() == 0) {
                        System.out.println("you have not selected any genres.");
                        running = false;
                    }

                    running = false;
                    System.out.println("your list has been saved!");
                }

                switch (input) {
                    case 1:
                        System.out.println("you added comedy");
                        genresChosen.add(genreList.get(0));
                        break;
                    case 2:
                        System.out.println("you have added action");
                        genresChosen.add(genreList.get(1));
                        break;
                    case 3:
                        System.out.println("you have added drama");
                        genresChosen.add(genreList.get(2));
                        break;
                    case 4:
                        System.out.println("you have added Thriller");
                        genresChosen.add(genreList.get(3));
                        break;
                    case 5:
                        System.out.println("you have added Horror");
                        genresChosen.add(genreList.get(4));
                        break;
                    case 6:
                        System.out.println("you have added Sci-fi");
                        genresChosen.add(genreList.get(5));
                        break;
                    case 7:
                        System.out.println("you have added Documentary");
                        genresChosen.add(genreList.get(6));
                        break;
                    case 8:
                        System.out.println("you have added Romance");
                        genresChosen.add(genreList.get(7));
                        break;
                    case 9:
                        System.out.println("you have added Indie");
                        genresChosen.add(genreList.get(8));
                        break;
                    case 10:
                        System.out.println("you have added Super Heros");
                        genresChosen.add(genreList.get(9));
                        break;
                }

            } else {
                System.out.println("sorry, please use keys from 1-10 and 0 " +
                        "to confirm");
            }
            StringBuffer gen = new StringBuffer();
            //hash set does not allow for duplicate values
            HashSet hs = new HashSet();
            hs.addAll(genresChosen);
            genresChosen.clear();
            genresChosen.addAll(hs);


                for(int i = 0; i > genresChosen.size(); i++) {
                    searchGenre(genresChosen.get(i));
                }

            if (genresChosen.size() == 5) {
                System.out.println("you have chosen: \n" + genresChosen);
                running = false;
            }
        }
        return message.toString();
    }

    public static String addToWatchedList(User user, Movie movie){
        StringBuffer message = new StringBuffer(ServiceDetails.ADDTOWATCHEDLIST + ServiceDetails.BREAKING_CHARACTERS);
        message.append(user.getId() + ServiceDetails.BREAKING_CHARACTERS + movie.getId() + ServiceDetails.BREAKING_CHARACTERS);

        output.println(message);
        output.flush();
        return message.toString();
    }


    //---------------------------------------------UTIL-------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------

    public static String printArr(List[] arr){
        String list = "";
        for(int i = 0; i < arr.length; i ++){
            list += "\n " +arr[i].toString();
        }
        return list;
    }

    public static String createJSONObject(Movie[] movie) {
        JSONObject jsonObj = new JSONObject(movie);
        System.out.println(jsonObj + "\n");
        return jsonObj.toString();
    }

    public static String createJSONObject(User[] user) {
        JSONObject jsonObj = new JSONObject(user);
        System.out.println(jsonObj + "\n");
        return jsonObj.toString();
    }

    public static void closeConnection() throws IOException {
        StringBuffer message = new StringBuffer(ServiceDetails.SESSION_TERMINATED);
        output.println(message);
        output.flush();

        dataSocket.close();
        out.close();

        response.equals(ServiceDetails.SESSION_TERMINATED);
        if (response.equals(ServiceDetails.SESSION_TERMINATED)) {
            System.out.println("--------------------");
            System.out.println("-  Session end     -");
            System.out.println("--------------------");
        }
    }
}
