package Core;

public class ServiceDetails {
    public static final int SERVER_PORT   = 50000;
    public static final String SERVER_IP =  "127.0.0.1";
    public static final String BREAKING_CHARACTERS = "%%";

    public static final int CLIENT_SERVER = 500;
    public static final int MAX_PAYLOAD   = 20;

    public static final String ENDSESSION   = "QUIT";
    public static final String LOGIN        = "LOGIN";
    public static final String REGISTERUSER = "REGISTERUSER";
    public static final String RECOMMEND    = "RECOMMEND";
    public static final String SEARCHMOVIE  = "SEARCHMOVIE";
    public static final String SEARCHGENRE  = "SEARCHGENRE";
    public static final String SEARCHDIRECTOR = "SEARCHDIRECTOR";
    public static final String FINDALLFILMS  = "FINDALLFILMS";
    public static final String FINDALLUSERS  = "FINDALLUSERS";
    public static final String FINDUSERBYNP = "FINDUSERBYNP";
    public static final String ADDMOVIE     = "ADDMOVIE";
    public static final String REMOVEMOVIE  = "REMOVEMOVIE";
    public static final String REMOVEUSER   = "REMOVEUSER";
    public static final String UPDATEFILM  = "UPDATEFILM";
    public static final String UPDATEUSER  = "UPDATEUSER";
    public static final String CHECKIFUSEREXISTS  = "CHECKIFUSEREXISTS";
    public static final String ADDTOWATCHEDLIST = "ADDTOWATCHEDLIST";
    public static final String VIEWWATCHEDLIST = "VIEWWATCHEDLIST";

    public static final String UNRECOGNIZED = "UNKNOWNCOMMAND";
    public static final String SESSION_TERMINATED = "GOODBYE";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";
}
