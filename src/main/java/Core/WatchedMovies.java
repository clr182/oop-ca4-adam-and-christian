package Core;

public class WatchedMovies {
    private int ID;
    private int movieID;
    private int userID;

    public WatchedMovies(int ID, int movieID, int userID) {
        this.ID = ID;
        this.movieID = movieID;
        this.userID = userID;
    }

    public WatchedMovies(int movieID, int userID) {
        this.ID = 0;
        this.movieID = movieID;
        this.userID = userID;
    }


    public int getID() {
        return ID;
    }

    public int getMovieID() {
        return movieID;
    }

    public int getUserID() {
        return userID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setMovieID(int movieID) {
        this.movieID = movieID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return "WatchedMovies{" +
                "ID=" + ID +
                ", movieID=" + movieID +
                ", userID=" + userID +
                '}';
    }
}
