package Core;


public class Movie
{
    private int id;
    private String title;
    private String genre;
    private String director;
    private String runtime;
    private String plot;
    private String location;
    private String rating;
    private String format;
    private String year;
    private String starring;
    private int copies;
    private String barcode;
    private String user_rating;


    public Movie(int id, String title, String genre, String director, String runtime, String plot, String location, String rating, String format, String year, String starring, int copies, String barcode, String user_rating)
    {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.runtime = runtime;
        this.plot = plot;
        this.location = location;
        this.rating = rating;
        this.format = format;
        this.year = year;
        this.starring = starring;
        this.copies = copies;
        this.barcode = barcode;
        this.user_rating = user_rating;
    }

    public Movie(String title, String genre, String director, String runtime, String plot, String location, String rating, String format, String year, String staring, int copies, String barcode, String user_rating)
    {
        this.id = 0;
        this.title           = title;
        this.genre          = genre;
        this.director       = director;
        this.runtime         = runtime;
        this.plot            = plot;
        this.location        = location;
        this.rating         = rating;
        this.format         = format;
        this.year            = year;
        this.starring        = staring;
        this.copies         = copies;
        this.barcode        = barcode;
        this.user_rating    = user_rating;
    }

    public Movie(){

    }

    public Movie(String title, String genre, String runtime, String rating, String year, String starring, String user_rating) {
        this.title           = title;
        this.genre          = genre;
        this.runtime         = runtime;
        this.starring       = starring;
        this.rating         = rating;
        this.year            = year;
        this.user_rating    = user_rating;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getDirector() {
        return director;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getPlot() {
        return plot;
    }

    public String getLocation() {
        return location;
    }

    public String getRating() {
        return rating;
    }

    public String getFormat() {
        return format;
    }

    public String getYear() {
        return year;
    }

    public String getStarring() {
        return starring;
    }

    public int getCopies() {
        return copies;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getUser_rating() {
        return user_rating;
    }


    @Override
    public String toString()
    {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", genre='" + genre + '\'' +
                ", director='" + director + '\'' +
                ", runtime=" + runtime +
                ", plot='" + plot + '\'' +
                ", location='" + location + '\'' +
                ", rating='" + rating + '\'' +
                ", format='" + format + '\'' +
                ", year=" + year +
                ", starring=" + starring +
                ", copies=" + copies +
                ", barcode=" + barcode +
                ", user_rating=" + user_rating +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) return false;

        Movie movie = (Movie) o;

        if (getId() != movie.getId()) return false;
        if (getCopies() != movie.getCopies()) return false;
        if (!getTitle().equals(movie.getTitle())) return false;
        if (!getGenre().equals(movie.getGenre())) return false;
        if (!getDirector().equals(movie.getDirector())) return false;
        if (!getRuntime().equals(movie.getRuntime())) return false;
        if (!getPlot().equals(movie.getPlot())) return false;
        if (!getLocation().equals(movie.getLocation())) return false;
        if (!getRating().equals(movie.getRating())) return false;
        if (!getFormat().equals(movie.getFormat())) return false;
        if (!getYear().equals(movie.getYear())) return false;
        if (!getStarring().equals(movie.getStarring())) return false;
        if (!getBarcode().equals(movie.getBarcode())) return false;
        return getUser_rating().equals(movie.getUser_rating());
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getTitle().hashCode();
        result = 31 * result + getGenre().hashCode();
        result = 31 * result + getDirector().hashCode();
        result = 31 * result + getRuntime().hashCode();
        result = 31 * result + getPlot().hashCode();
        result = 31 * result + getLocation().hashCode();
        result = 31 * result + getRating().hashCode();
        result = 31 * result + getFormat().hashCode();
        result = 31 * result + getYear().hashCode();
        result = 31 * result + getStarring().hashCode();
        result = 31 * result + getCopies();
        result = 31 * result + getBarcode().hashCode();
        result = 31 * result + getUser_rating().hashCode();
        return result;
    }
}
