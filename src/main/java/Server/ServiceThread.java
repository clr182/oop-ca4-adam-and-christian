package Server;

import Core.Movie;
import Core.ServiceDetails;
import Core.User;
import Exceptions.DAOException;
import org.json.JSONObject;

import javax.xml.ws.Service;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.net.Socket;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static Client.Client.printArr;

public class ServiceThread extends Thread {
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;

    UserDAOInterface IUserDAO = new MySqlUserDAO();
    MovieDAOInterface IMovieDAO = new MysqlMovieDAO();
    WatchedMoviesDAOInterface IWatchedDAO = new MysqlWatchedMovieDAO();

    public ServiceThread(ThreadGroup group, String name,
                         Socket dataSocket, int count){
                                super(group, name);

        try{
            this.dataSocket = dataSocket;
            this.number = count;
            input = new Scanner(new InputStreamReader(
                    this.dataSocket.getInputStream()));
            output = new PrintWriter(this.dataSocket.getOutputStream(), true);
            }
        catch(IOException iox){
            iox.printStackTrace();
        }
    }

    @Override
    public void run() {
        String incomingMessage = "";
        String response;

        try {
            while (!incomingMessage.equals(ServiceDetails.ENDSESSION)) {
                response = "";
                incomingMessage = input.nextLine();
                System.out.println("Message Recieved: " + incomingMessage);

                String[] componants = incomingMessage.split
                        (ServiceDetails.BREAKING_CHARACTERS);

                if (componants[0].equals(ServiceDetails.LOGIN)) {
                    IUserDAO.findUserByUsernamePassword(componants[1], componants[2]);
                    response = "user signed in as: " + componants[1];
                    output.println(response);
                    output.flush();

                }if (componants[0].equals(ServiceDetails.FINDUSERBYNP)) {
                    response = String.valueOf(IUserDAO.findUserByUsernamePassword(componants[1], componants[2]));
                    output.println(response);
                    output.flush();

                }if (componants[0].equals(ServiceDetails.FINDALLFILMS)) {
                    response = createJSONObject(IMovieDAO.findAllMovies());
                    output.println(response);
                    output.flush();

                } if (componants[0].equals(ServiceDetails.FINDALLUSERS)) {
                    User[] user = IUserDAO.findAllUsers();
                    response = createJSONObject(user);
                    output.println(response);
                    output.flush();

                } if(componants[0].equals(ServiceDetails.CHECKIFUSEREXISTS)){
                    response = String.valueOf(IUserDAO.checkUserExists(componants[1], componants[2]));
                    output.println(response);
                    output.flush();
                }
                if(componants[0].equals(ServiceDetails.ADDTOWATCHEDLIST)){
                    User user = new User(componants[1], componants[2], componants[3], componants[4]);
                    Movie movie = new Movie(componants[5], componants[6], componants[7],componants[8], componants[9],
                            componants[10], componants[11]);
                    response = IWatchedDAO.addToUsersWatchedlist(user.getId(), movie.getId());

                    output.println(response);
                    output.flush();
                }

                if (componants[0].equals(ServiceDetails.RECOMMEND)) {
                    response = createJSONObject(IMovieDAO.searchMovie(componants[1]));
                    output.println(response);
                    output.flush();

                } if (componants[0].equals(ServiceDetails.REMOVEUSER)) {
                    response = IUserDAO.removeUser(componants[1], componants[2]);
                    output.println(response);
                    output.flush();

                } if (componants[0].equals(ServiceDetails.UPDATEFILM)) {
                    response = IMovieDAO.updateMovie(componants[1]).toString();
                    output.println(response);
                    output.flush();

                } if (componants[0].equals(ServiceDetails.UPDATEUSER)) {
                    response = IUserDAO.updateUser(componants[1], componants[2], Integer.parseInt(componants[3]));
                    output.println(response);
                    output.flush();

                } if (componants[0].equals(ServiceDetails.REGISTERUSER)) {
                    User regUser = IUserDAO.addNewUser(componants[1], componants[2], componants[3], componants[4]);
                    response = regUser.toString();
                    output.println(response);
                    output.flush();

                } if (componants[0].equals(ServiceDetails.SEARCHGENRE)) {
                    response = createJSONObject(IMovieDAO.searchGenre(componants[1]));
                    output.println(response);
                    output.flush();

                } if (componants[0].equals(ServiceDetails.ADDMOVIE)){
                    Movie mov = IMovieDAO.addMovie(componants[1], componants[2], componants[3], componants[4],
                            componants[5], componants[6], componants[7], componants[8], componants[9], componants[10],
                            Integer.parseInt(componants[11]));

                    response = mov.toString();
                    output.println(response);
                    output.flush();
                }
                if (componants[0].equals(ServiceDetails.ENDSESSION)) {
                    response = ServiceDetails.SESSION_TERMINATED;
                    output.println(response);
                    output.flush();
                }
                else {
                    response = ServiceDetails.UNRECOGNIZED;
                    output.println(response);
                    output.flush();
                }

                output.println(response);
                output.flush();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        catch (DAOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }  finally {
            try {
                dataSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
        public static String createJSONObject(Movie[] movie) {
        JSONObject jsonObj = new JSONObject(movie);
        System.out.println(jsonObj + "\n");
        return jsonObj.toString();
    }

    public static String createJSONObject(User[] user) {
        JSONObject jsonObj = new JSONObject(user);
        System.out.println(jsonObj + "\n");
        return jsonObj.toString();
    }


}
