package Server;

import Exceptions.DAOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlDAO {
    public Connection getConnection() throws DAOException {
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/movie_database";
        String username = "root";
        String password = "";
        Connection con = null;

        try{
            con = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            System.out.println("Connection failed: " + e.getMessage());
            System.exit(1);
        }
        return con;
    }

    public void closeConnection(Connection con) throws DAOException{
        try{
            if(con != null){
                con.close();
                con = null;
            }
        } catch (SQLException e) {
            System.out.println("Failed to free the connection: " + e.getMessage());
            System.exit(1);
        }
    }
}
