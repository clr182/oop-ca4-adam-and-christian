package Server;

import Core.Movie;
import Exceptions.DAOException;

import java.sql.SQLException;
import java.util.List;

public interface MovieDAOInterface
{
    public Movie[] findAllMovies() throws Exceptions.DAOException;
    public Movie[] searchDirector(String Director) throws Exceptions.DAOException, SQLException;
    public Movie addMovie(String TITLE, String GENRE, String DIRECTOR, String RUNTIME, String PLOT, String LOCATION,String RATING,String FORMAT,String YEAR,String STARRING,int COPIES) throws DAOException, SQLException;
    public Movie[] searchMovie(String movieTitle) throws DAOException, SQLException;
    public Movie[] searchGenre(String movieGenre) throws DAOException, SQLException;
    public Movie removeMovie(String movieTitle) throws DAOException, SQLException;
    public Movie updateMovie(String movieTitle) throws  SQLException;
}