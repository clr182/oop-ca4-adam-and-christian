package Server;

import Core.Movie;
import Core.ServiceDetails;
import Core.User;
import Exceptions.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MySqlUserDAO extends MysqlDAO implements UserDAOInterface {

    public User[] findAllUsers() throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = this.getConnection();
            String SELECTALLquery = "SELECT * FROM `user`";
            ps = con.prepareStatement(SELECTALLquery);
            rs = ps.executeQuery();
            User[] resultsSet = new User[rs.getRow()];

            while (rs.next()) {
                resultsSet[rs.getRow() - 1] = new User(
                        rs.getString("USERNAME"),
                        rs.getString("PASSWORD"),
                        rs.getString("LAST_NAME"),
                        rs.getString("FIRST_NAME"));
            }
            return resultsSet;

        } catch (SQLException e) {
            throw new DAOException("findAllUsers() " + e.getMessage());
        } finally {
            try {
                if (rs != null) {

                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.closeConnection(con);
                }
            } catch (SQLException e) {
                throw new DAOException("findAllUsers() " + e.getMessage());
            }
        }
    }

    public User findUserByUsernamePassword(String uname, String pword) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User u = null;

        try {
            //Get connection to the DB
            con = this.getConnection();
            String query = "SELECT * FROM USER WHERE USERNAME = ? AND PASSWORD = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, uname);
            ps.setString(2, pword);

            //Use the prepared statement to execute SQL
            rs = ps.executeQuery();
            if (rs.next()) {
                int userId = rs.getInt("ID");
                String username = rs.getString("USERNAME");
                String password = rs.getString("PASSWORD");
                String lastname = rs.getString("LAST_NAME");
                String firstname = rs.getString("FIRST_NAME");
                u = new User(userId, firstname, lastname, username, password);
            }

        } catch (SQLException e) {
            throw new DAOException("findUserByUsernamePassword() " + e.getMessage());
        } finally {
            try {
                if (rs.next()) {
                    System.out.println("success");
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.closeConnection(con);
                }

            } catch (SQLException e) {
                throw new DAOException("findUserByUsernamePassword() " + e.getMessage());
            }
        }
        return u;
    }

    public boolean checkUserExists(String uname, String pword) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User uToCompare = new User(uname, pword);
        User u = null;
        boolean rtrn = true;

        try {
            //Get connection to the DB
            con = this.getConnection();
            String query = "SELECT * FROM USER WHERE USERNAME = ? AND PASSWORD = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, uname);
            ps.setString(2, pword);

            //Use the prepared statement to execute SQL
            rs = ps.executeQuery();
            if (rs.next()) {
                String username = rs.getString("USERNAME");
                String password = rs.getString("PASSWORD");
                u = new User(username, password);
            }


        } catch (SQLException e) {
            throw new DAOException("findUserByUsernamePassword() " + e.getMessage());
        } finally {
            closeConn(con, ps);
    }


        if(u.getUsername().equals(uToCompare.getUsername())){
            System.out.println("user signed in!!!");
            rtrn = true;
        }
        else{
            System.out.println("sorry user does not exist");
            rtrn = false;
        }

        return rtrn;
    }

    public User addNewUser(String username, String password, String firstname, String lastname) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        User u = null;

        try {
            //Get connection to the DB
            con = this.getConnection();
            String query = "INSERT INTO `user`(`USERNAME`, `PASSWORD`, `LAST_NAME`, `FIRST_NAME`) VALUES ('" + username + "','" + password + "','" + firstname + "','" + lastname + "')";
            ps = con.prepareStatement(query);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("AddMovies()" + e.getMessage());
        }finally {
            closeConn(con, ps);

        }
        return null;
    }

    public String updateUser(String userToUpdate, String update, int inputchoice) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = this.getConnection();

            switch(inputchoice) {
                case(1):
                    String updateUsernameQuery = "UPDATE `user` SET `USERNAME` = '" + update + "' WHERE username = '" + userToUpdate + "'";
                    ps = con.prepareStatement(updateUsernameQuery);
                    ps.executeUpdate();
                    break;
                case(2):
                    String updatePaswordQuery = "UPDATE `user` SET `PASSWORD` = '" + update + "' WHERE username = '" + userToUpdate + "'";
                    ps = con.prepareStatement(updatePaswordQuery);
                    ps.executeUpdate();
                    break;
                case(3):
                    String updateFirstNameQuery = "UPDATE `user` SET `FIRST_NAME`='" + update + "'  WHERE username = '" + userToUpdate + "'";
                    ps = con.prepareStatement(updateFirstNameQuery);
                    ps.executeUpdate();
                    break;
                case(4):
                    String updateLastNameQuery = "UPDATE `user` SET `LAST_NAME`='" + update + "'  WHERE username = '" + userToUpdate + "'";
                    ps = con.prepareStatement(updateLastNameQuery);
                    ps.executeUpdate();
                    break;
                }
            System.out.println("Updated!");
            //Get connection to the DB
        } catch (SQLException e) {
            throw new DAOException("updateUser()" + e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.closeConnection(con);
                }
            } catch (SQLException e) {
                throw new DAOException("updateUser()" + e.getMessage());
            }
        }
        return userToUpdate + " updated";
    }

    public String removeUser(String userToDelete, String passToDelete) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        List<User> users = new ArrayList<User>();

        try {

            //Get connection to the DB
            con = this.getConnection();

            String deleteQuery = "DELETE FROM `user` WHERE USERNAME LIKE '" + userToDelete + "'  && PASSWORD LIKE '" + passToDelete +"'";

            ps = con.prepareStatement(deleteQuery);
            ps.executeUpdate();
            System.out.println("Thank you for deleting: " + userToDelete);
        } catch (SQLException e) {
            throw new DAOException("removeUser()" + e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.closeConnection(con);
                }
            } catch (SQLException e) {
                throw new DAOException("removeUser()"  + e.getMessage());
            }
        }
        return ServiceDetails.SUCCESS;
    }

    public void closeConn(Connection con, PreparedStatement ps) throws DAOException {
        try {
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                closeConnection(con);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        }
    }

}
