package Server;

import Core.ServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        try{
            //Create a listening Socket
            ServerSocket listeningSocket = new ServerSocket
                    (ServiceDetails.SERVER_PORT);

            //Create a ThreadGroup
            ThreadGroup group = new ThreadGroup("ClientThreads");
            //accepting threads over processing threads
            group.setMaxPriority(Thread.currentThread().getPriority()-1);

            boolean continueRunning = true;
            int threadCount = 0;


            while(continueRunning){
                Socket dataSocket = listeningSocket.accept();

                threadCount++;

                ServiceThread newClient = new
                        ServiceThread(group, dataSocket.getInetAddress()+"",
                    dataSocket, threadCount);

                newClient.start();
            }
            listeningSocket.close();

        }catch(NullPointerException e){
            System.out.println("null");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
