package Server;

import Core.Movie;
import Exceptions.DAOException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class MysqlMovieDAO extends MysqlDAO implements MovieDAOInterface {


    public Movie[] findAllMovies() throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        try {

            ResultSet rs;
            //Get connection to the DB
            con = this.getConnection();
            String query = "SELECT * FROM `movies` LIMIT 10";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            Movie[] resultsSet = new Movie[rs.getRow()];
            while (rs.next()) {
                resultsSet[rs.getRow() - 1] = new Movie(
                rs.getInt("ID"),
                rs.getString("title"),
                rs.getString("genre"),
                rs.getString("director"),
                rs.getString("runtime"),
                rs.getString("plot"),
                rs.getString("location"),
                rs.getString("rating"),
                rs.getString("format"),
                rs.getString("year"),
                rs.getString("starring"),
                rs.getInt("copies"),
                rs.getString("barcode"),
                rs.getString("user_rating"));
            }
            return resultsSet;
        } catch (SQLException e) {
            throw new DAOException("FindAllMovies()" + e.getMessage());
        } finally {
            closeConn(con, ps);
        }

    }

    public Movie addMovie(String TITLE, String GENRE, String DIRECTOR, String RUNTIME, String PLOT, String LOCATION, String RATING, String FORMAT,
                          String YEAR, String STARRING, int COPIES) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        List<Movie> movies = new ArrayList<Movie>();
        System.out.println("Hey you would like to add a movie, please provide the following details!");
        try {
            //Get connection to the DB
            con = this.getConnection();
            String query = "INSERT INTO MOVIES(`title`, `genre`, `director`, `runtime`, `plot`, `location`, `rating`, `format`, `year`, `starring`) " +
                    "VALUES ('" + TITLE + "', '" +GENRE + "', '" + DIRECTOR+ "','" + RUNTIME + "','"+ PLOT+ "','"+ LOCATION+ "','"+RATING+"','"+FORMAT+"','"+YEAR+"','"+STARRING+ "') ";
            ps = con.prepareStatement(query);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("AddMovies()" + e.getMessage());
        } finally {
            closeConn(con, ps);
        }
        return null;
    }

    public Movie[] searchMovie(String movieTitle) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;

        ResultSet rs = null;
        List<Movie> movies = new ArrayList<Movie>();

        try{
            String queryTitle = "SELECT * FROM `MOVIES` WHERE title like '" +  movieTitle  +"%' LIMIT 15 ";

            ps = con.prepareStatement(queryTitle);
            rs = ps.executeQuery();
            Movie[] resultsSet = new Movie[rs.getRow()];
            while (rs.next()) {
                resultsSet[rs.getRow() - 1] = new Movie(
                rs.getString("title"),
                rs.getString("genre"),
                rs.getString("runtime"),
                rs.getString("rating"),
                rs.getString("year"),
                rs.getString("starring"),
                rs.getString("user_rating"));
            }
            return resultsSet;

        } catch (SQLException e) {
            throw new DAOException("searchMovies()" + e.getMessage());
        } finally {
            closeConn(con, ps);
        }
    }

    public Movie[] searchDirector(String movieDirector) throws DAOException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Movie> movies = new ArrayList<Movie>();
        try{
            String queryTitle = "SELECT * FROM `MOVIES` WHERE director like '" + movieDirector + "%' ORDER BY director ASC LIMIT 15";

            ps = con.prepareStatement(queryTitle);
            rs = ps.executeQuery();
            Movie[] resultsSet = new Movie[rs.getRow()];
            while (rs.next()) {
                resultsSet[rs.getRow() - 1] = new Movie(
                        rs.getString("title"),
                        rs.getString("genre"),
                        rs.getString("runtime"),
                        rs.getString("rating"),
                        rs.getString("year"),
                        rs.getString("starring"),
                        rs.getString("user_rating"));
            }
            return resultsSet;

        } catch (SQLException e) {
            throw new DAOException("searchMovies()" + e.getMessage());
        } finally {
            closeConn(con, ps);
        }
    }

    public Movie[] searchGenre(String movieGenre) throws DAOException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        try{
            String queryTitle = "SELECT * FROM `movies` WHERE genre like '" + movieGenre + "%' ORDER BY id DESC LIMIT 10";
            ps = con.prepareStatement(queryTitle);
            rs = ps.executeQuery();
            Movie[] resultsSet = new Movie[rs.getRow()];
            while (rs.next()) {
                resultsSet[rs.getRow() - 1] = new Movie(
                        rs.getString("title"),
                        rs.getString("genre"),
                        rs.getString("runtime"),
                        rs.getString("rating"),
                        rs.getString("year"),
                        rs.getString("starring"),
                        rs.getString("user_rating"));
            }
            return resultsSet;

        } catch (SQLException e) {
            throw new DAOException("searchMovies()" + e.getMessage());
        } finally {
            closeConn(con, ps);
        }
    }

    public Movie removeMovie(String movieTitle) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        List<Movie> movies = new ArrayList<Movie>();

        try {
            //Get connection to the DB
            con = this.getConnection();

            String deleteQuery = "DELETE FROM `MOVIES` WHERE title LIKE '" + movieTitle + "' ";

            ps = con.prepareStatement(deleteQuery);
            ps.executeUpdate();
            System.out.println("Thank you for deleting: " + movieTitle);
        } catch (SQLException e) {
            throw new DAOException("removeMovies()" + e.getMessage());
        } finally {
            closeConn(con, ps);
        }

        return null;
    }

    public Movie updateMovie(String movieTitle) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        List<Movie> movies = new ArrayList<Movie>();

        try{
            //Get connection to the DB
            con = this.getConnection();
            Scanner sc = new Scanner(System.in);
            System.out.println();
            System.out.println("now update " + movieTitle + "!");

            System.out.println("set new: \n");
            System.out.println("title:");    String title = sc.nextLine();
            System.out.println("genre:");    String genre = sc.nextLine();
            System.out.println("director:"); String director= sc.nextLine();
            System.out.println("runtime:");  String runtime = sc.nextLine();
            System.out.println("plot:");     String plot = sc.nextLine();
            System.out.println("location:"); String location = sc.nextLine();
            System.out.println("rating:");   String rating = sc.nextLine();
            System.out.println("format:");   String format = sc.nextLine();
            System.out.println("year:");     String year = sc.nextLine();
            System.out.println("starring:"); String starring = sc.nextLine();
            System.out.println("copies:");   int copies = sc.nextInt();


            String updateQuery = "UPDATE `movies` SET `title`='" + title + "',`genre`='" + genre + "',`director`='" +director +"',`runtime`='"+runtime+"',`plot`='"+plot+"'," +
                    "`location`='"+location+"',`rating`='"+rating+"',`format`='"+format+"',`year`='"+year+"',`starring`='"+starring+"',`copies`='"+copies+"'" +
                    " WHERE title like '" + movieTitle + "'";

            ps = con.prepareStatement(updateQuery);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("updateMovies()" + e.getMessage());
        } finally {
            closeConn(con, ps);
        }
        return null;
    }

    public void closeConn(Connection con, PreparedStatement ps) throws DAOException {
        try {
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                closeConnection(con);
            }
        } catch (SQLException e) {
            throw new DAOException("searchMovies()"  + e.getMessage());
        }
    }

}