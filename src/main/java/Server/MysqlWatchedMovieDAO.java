
package Server;

import Core.User;
import Core.WatchedMovies;
import Exceptions.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MysqlWatchedMovieDAO extends MysqlDAO implements WatchedMoviesDAOInterface {

    public String addToUsersWatchedlist(int userID, int movieID) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        WatchedMovies watchedMovies = null;

        try {
            //Get connection to the DB
            con = this.getConnection();
            String query = "INSERT INTO `watchedmovies`(`movieID`, `userID`) VALUES ('" + userID + "','" + movieID + "')";
            ps = con.prepareStatement(query);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("AddMovies()" + e.getMessage());
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    closeConnection(con);
                }
            } catch (SQLException e) {
                throw new DAOException(e.getMessage());
            }
        }
        return "added to watch list";
    }

    public String viewWatchedList(User user) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = this.getConnection();
            String SELECTALLquery = "SELECT * FROM `watchedmovies`";
            ps = con.prepareStatement(SELECTALLquery);
            rs = ps.executeQuery();
            WatchedMovies[] resultsSet = new WatchedMovies[rs.getRow()];

            while (rs.next()) {
                resultsSet[rs.getRow() - 1] = new WatchedMovies(
                        rs.getInt("ID"),
                        rs.getInt("movieID"),
                        rs.getInt("userID"));
            }
            return resultsSet.toString();

        } catch (SQLException e) {
            throw new DAOException("viewWatchedList() " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.closeConnection(con);
                }
            } catch (SQLException e) {
                throw new DAOException("viewWatchedList() " + e.getMessage());
            }
        }
    }
    }
