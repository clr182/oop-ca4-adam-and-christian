package Server;


import Core.User;
import Core.WatchedMovies;
import Exceptions.DAOException;


public interface WatchedMoviesDAOInterface {
    public String addToUsersWatchedlist(int userID, int movieID) throws DAOException;
    public String viewWatchedList(User user) throws  DAOException;
}
