package Server;

import Core.User;
import Exceptions.DAOException;

import java.sql.SQLException;
import java.util.List;

public interface UserDAOInterface {
    public User[] findAllUsers() throws Exceptions.DAOException;
    public User findUserByUsernamePassword(String username, String password) throws DAOException;
    public boolean checkUserExists(String username, String password) throws DAOException;
    public User addNewUser(String username, String password, String firstname, String lastname) throws DAOException;
    public String updateUser(String userToUpdate, String update, int choice) throws SQLException;
    public String removeUser(String userToDelete, String passToDelete) throws SQLException;
}
