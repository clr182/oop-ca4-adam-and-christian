# Movie Extraction Software


## What is this?
    This is a Object-Oriented, user-interactive, movie database application.
    We have constructed a Client-Server TCP connection which enables a 
    client to Log-in and query the the server (add, delete, update, recommend) 
    movie database.

## Technologies used:
    - Classes and Objects,
    - MySQL Database,
    - DTOs,
    - DAOs,
    - Interfaces,
    - Threads per client,
    - TCP (Client-Server connection),
    - User-Friendly menu interface.
    - Database queries,
    - Exceptions,
    - Input Validation,
    - Appropriate Testing.

## Developed by:
  `Adam Zieba` & `Christian Rafferty` 

## References
    We would like to reference John Loane for the starter DAO and DTO code. 
