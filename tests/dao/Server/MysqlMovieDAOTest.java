//package Server;
//
//import Client.Client;
//import Core.User;
//import org.jboss.arquillian.container.test.api.Deployment;
//import org.jboss.arquillian.junit.Arquillian;
//import org.jboss.shrinkwrap.api.ShrinkWrap;
//import org.jboss.shrinkwrap.api.asset.EmptyAsset;
//import org.jboss.shrinkwrap.api.spec.JavaArchive;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import static org.junit.Assert.*;
//
//@RunWith(Arquillian.class)
//public class MysqlMovieDAOTest {
//    @Deployment
//    public static JavaArchive createDeployment() {
//        return ShrinkWrap.create(JavaArchive.class)
//                .addClass(MysqlMovieDAO.class)
//                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
//    }
//
//    @Before
//    public void setUp() throws Exception { }
//
//    @After
//    public void tearDown() throws Exception {
//    }
//
//    @Test
//    public void testToString()
//    {
//        System.out.println("toString");
//        MysqlMovieDAO instance = new MysqlMovieDAO("title", "genre", "director", "runtime", "plot", "location", "rating", "format", "year", "starring", 2, "barcode", "user_rating");
//        String expResult = "Movie{id=0, title=title, genre=genre, director=director, runtime=runtime, plot=plot, location=location, poster=poster, rating=rating, format=format, year=year, starring=starring, copies=2, barcode=barcode, user_rating=user_rating}";
//        String result = instance.toString();
//        assertEquals(expResult, result);
//    }
//
//
//}
